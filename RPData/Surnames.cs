﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPData
{
    class Surnames
    {
        public List<string> surnames = new List<string>()
        {
            "Szczepkowski",
            "Kapłon",
            "Frańczak",
            "Gruszka",
            "Kin",
            "Zimny",
            "Wrześniewski",
            "Wacławik",
            "Łyżwa",
            "Dera",
            "Jurek",
            "Twardowski",
            "Kraśnicka",
            "Kapuściński",
            "Łucki",
            "Ociepka",
            "Jankowiak",
            "Szczurek",
            "Wałach",
            "Łojko",
            "Stefaniak",
            "Kozicka",
            "Kuś",
            "Wojtanowski",
            "Szefler",
            "Ciura",
            "Gdaniec",
            "Zarzecki",
            "Wlazło",
            "Waś",
            "Kot",
            "Kościuczyk",
            "Bosko",
            "Kalinowska",
            "Samociuk",
            "Piekarska",
            "Sawicz",
            "Sawicki",
            "Gajownik",
            "Taszakowska",
            "Taszlińska",
            "Trupkiewicz",
            "Trybek",
            "Torwiński",
            "Korzeniowski",
            "Głowacki",
            "Kubaczyński",
            "Sójka",
            "Sawicki",
            "Czopek",
            "Szwed",
            "Romaniuk",
            "Magier",
            "Wojtaś",
            "Kot",
            "Dymek",
            "Kobojek",
            "Mazurek",
            "Koczkodaj",
            "Kucio",
            "Abarbanel",
            "Adler",
            "Baruch",
            "Berman",
            "Chajek",
            "Duszejkin",
            "Grynszpan",
            "Hadad",
            "Himelfarb",
            "Horowicz",
            "Izraelewski",
            "Fiszel",
            "Fróg",
            "Gierszonek",
            "Goldberg",
            "Rabinowicz",
            "Rapoport",
            "Sagał",
            "Kaufman",
            "Szapiro",
            "Mełamed",
            "Zemel",
            "Kranz",
            "Margolis",
            "Zalcer",
            "Mandel",
            "Fogler",
            "Rubinstein",
            "Wermand",
            "Wielmożny",
            "Soborski",
            "Radziłowski",
            "Wielkomiski",
            "Paśkudzki",
            "Markoański",
            "Borkowiecki",
            "Morkowski",
            "Radzimowy",
            "Gmółka",
            "Sakiewski",
            "Hamor",
            "Boczek",
            "Pażdzioch",
            "Kaczorek",
            "Planeta",
            "Ramowska",
            "Olszewska",
            "Walikoń",
            "Tchurz",
            "Kot",
            "Betka",
            "Sześciokar",
            "Doktor",
            "Pedowolski",
            "Pedał",
            "Matkował",
            "Kręciworko",
            "Wałowa",
            "Hermud",
            "Młotowalny",
        };
    }
}
